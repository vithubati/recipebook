import { ShoppingListService } from './shopping-list.service';
import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { Ingredient } from './../recipes/ingredient';

@Component({
  selector: 'rb-shopping-list-add',
  templateUrl: './shopping-list-add.component.html'
})
export class ShoppingListAddComponent implements OnChanges {
  @Input() item: Ingredient;
  @Output() cleared = new EventEmitter();
  isAdd = true;
  
  constructor(private sLS: ShoppingListService) { }

  ngOnChanges(changes) {
    if(changes.item.currentValue === null){
      this.isAdd = true;
      this.item = {name: null, amount: null}
    }else{
      this.isAdd = false;
    }
  }

  onSubmit(incredient: Ingredient) {
    const newIngredient = new Ingredient(incredient.name, incredient.amount);
    if(!this.isAdd){
      this.sLS.editItem(this.item, newIngredient);
      this.onClear();
    }else{
      this.item = newIngredient;
      this.sLS.addItem(this.item);
    }
  }

  onDelete(){
    this.sLS.deleteItem(this.item);
    this.onClear();
  }

  onClear(){
    this.isAdd = true;
    this.cleared.emit(null);
  }
}
