import { Injectable } from '@angular/core';
import { Recipe } from './recipe';
import { Ingredient } from './ingredient';

@Injectable()
export class RecipeService {
  private allRecipes: Recipe[] = [
    new Recipe('Burger', 'Very tasty', 'http://vignette1.wikia.nocookie.net/ronaldmcdonald/images/c/c7/1955_Burger.png/revision/latest?cb=20151206183328', [
      new Ingredient('French Fries', 2),
      new Ingredient('Pork Meat', 1)
    ]),
    new Recipe('Summer salads', 'Delicious', 'http://www.pngall.com/wp-content/uploads/2016/05/Salad.png', [])
  ];
  constructor() { }

  getRecipes(){
    return this.allRecipes;
  }

  getRecipe(id: number ){
    return this.allRecipes[id];
  }
  deleteRecipe(recipe: Recipe){
    this.allRecipes.splice(this.allRecipes.indexOf(recipe), 1);
  }
}
